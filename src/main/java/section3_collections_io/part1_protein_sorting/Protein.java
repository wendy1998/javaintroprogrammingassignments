/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section3_collections_io.part1_protein_sorting;

import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    private static final String[] legalAminoAcids = new String[] {"I", "L", "K", "M", "F", "T", "W", "V", "R", "H",
            "A", "N", "D", "C", "E", "Q", "G", "P", "S", "Y"};
    private static HashMap<String, Double> aaWeights = createAminoAcidWeightMap();
    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    public final double proteinWeight;
    private GOannotation goAnnotation;
    
    private static HashMap<String, Double> createAminoAcidWeightMap() {
        HashMap<String, Double> aaMap = new HashMap<>();
        final Double[] weights = new Double[] {131.1736, 131.1736, 146.1882,
                149.2124, 165.1900, 119.1197, 204.2262, 117.1469,
                174.2017, 155.1552, 89.0935, 132.1184, 133.1032,
                121.1590, 147.1299, 146.1451, 75.0669, 115.1310,
                105.0930, 181.1894};

        int i;
        for (i=0; i < weights.length; i++) {
            aaMap.put(legalAminoAcids[i], weights[i]);
        }

        return aaMap;
    }

    private double calculateProteinWeight() {
        double weight = 0;
        for(String aa : this.aminoAcidSequence.split("")){
            weight += aaWeights.get(aa);
        }
        return weight;
    }

    /**
     * constructs without GO annotation;
     * @param name
     * @param accession
     * @param aminoAcidSequence 
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.proteinWeight = calculateProteinWeight();
    }

    /**
     * construicts with main properties.
     * @param name
     * @param accession
     * @param aminoAcidSequence
     * @param goAnnotation 
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.goAnnotation = goAnnotation;
        this.proteinWeight = calculateProteinWeight();
    }

    @Override
    public int compareTo(Protein o) {
        return this.name.compareTo(o.name);
    }

    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) throws IllegalArgumentException {
        if (type == null) {
            throw new IllegalArgumentException("Wrong type");
        }
        int flag = 0;
        for (SortingType t : SortingType.values()) {
            if (t.name().equals(type.name())) {
                flag = 1;
            }
        }
        if(flag == 0) {
            throw new IllegalArgumentException("Wrong type");
        }
        switch (type) {
            case ACCESSION_NUMBER: {
                return new Comparator<Protein>() {
                    @Override
                    public int compare(Protein o1, Protein o2) {
                        return o1.getAccession().toLowerCase().compareTo(o2.getAccession().toLowerCase());
                    }
                };
            }
            case GO_ANNOTATION: {
                return new Comparator<Protein>() {
                    @Override
                    public int compare(Protein o1, Protein o2) {
                        /*sort on biological process*/
                        int comp = o1.goAnnotation.getBiologicalProcess().compareTo(o2.goAnnotation.getBiologicalProcess());
                        /*sort on cellular component*/
                        if (comp == 0) {
                            comp = o1.goAnnotation.getCellularComponent().compareTo(o2.goAnnotation.getCellularComponent());
                        } else {
                            return comp;
                        }
                        /*sort on molecular function*/
                        if (comp == 0) {
                            return o1.goAnnotation.getMolecularFunction().compareTo(o2.goAnnotation.getMolecularFunction());
                        } else {
                            return comp;
                        }
                    }
                };
            }
            case PROTEIN_WEIGHT: {
                return new Comparator<Protein>() {
                    @Override
                    public int compare(Protein o1, Protein o2) {
                        return -1*Double.compare(o1.proteinWeight, o2.proteinWeight);
                    }
                };
            }
            default: {
                return new Comparator<Protein>() {
                    @Override
                    public int compare(Protein p1, Protein o) {
                        return p1.getName().compareTo(o.getName());
                    }
                };
            }
        }
    }

    /**
     *
     * @return name the name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }


}
