/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Elephant extends Animal {

    Elephant(int age) {
        this.age = age;
        this.maxSpeed = 40;
        this.maxAge = 86;
    }
    @Override
    public String getName() {
        return "Elephant";
    }

    @Override
    public String getMovementType() {
        return "thunder";
    }
}
