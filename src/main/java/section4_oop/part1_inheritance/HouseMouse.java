/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal{

    HouseMouse(int age) {
        this.age = age;
        this.maxSpeed = 21;
        this.maxAge = 13;
    }
    @Override
    public String getName() {
        return "Mouse";
    }

    @Override
    public String getMovementType() {
        return "scurry";
    }
}
