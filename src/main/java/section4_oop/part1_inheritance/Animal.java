/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public abstract class Animal {
    int maxAge;
    double maxSpeed;
    int age;

    private double calculateSpeed() {
        return maxSpeed * (0.5 + (0.5 * (((double)maxAge - age)/maxAge)));
    }

    /**
     * returns the name of the animal
     * @return name the species name
     */
    public abstract String getName();
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public abstract String getMovementType();
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {return calculateSpeed();}

    /**
     * returns the maximum age of this animal
     * @return maxAge the maximum age of this animal
     */
    public int getMaxAge() {return maxAge;}

    /**
     * returns the maximum speed of this animal
     * @return maxSpeed the maximum speed of this animal
     */
    public double getMaxSpeed() {return maxSpeed;}
    
}
