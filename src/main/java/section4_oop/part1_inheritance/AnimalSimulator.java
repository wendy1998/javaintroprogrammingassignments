/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }

    
    private void start(String[] args) {
        //start processing command line arguments
        /*check whether arguments are provided*/
        if(args.length == 0) {
            printHelp();
            return;
        }
        /*is help is asked, print it*/
        if(args[0].equals("help")){
            printHelp();
            return;
        }
        /*check if commandline args are alright*/
        try {
            checkCommandlineArgs(args);
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }
        /*if commandline args are all right, create animal object*/
        for (int i = 0; i < args.length; i += 2) {
            String animalName = args[i].toLowerCase();
            int age = Integer.parseInt(args[i+1]);

            Animal animal;
            switch (animalName) {
                case "elephant":
                    animal = new Elephant(age);
                    break;
                case "horse":
                    animal = new Horse(age);
                    break;
                case "mouse":
                    animal = new HouseMouse(age);
                    break;
                default:
                    animal = new Tortoise(age);
            }

            /*Check if right age is provided*/
            try {
                checkMaxAge(animal, age);
            } catch(IllegalArgumentException e) {
                System.out.println(e.getMessage());
                return;
            }

            /*Check if speed is not too fast*/
            try {
                checkMaxSpeed(animal, animal.getSpeed());
            } catch(IllegalArgumentException e) {
                System.out.println(e.getMessage());
                return;
            }

            /*if everything is alright, print result*/
            printResult(animal);
        }
    }

    private void printResult(Animal animal) {
        String name = animal.getName();
        String prefix;
        if(name.equals("Elephant")){
            prefix = "An ";
        } else {
            prefix = "A ";
        }
        double speed = Math.round(animal.getSpeed()*10)/10.0;
        int age = animal.age;
        String movementType = animal.getMovementType();

        System.out.println(prefix + name + " of age " + age +
                " moving in " + movementType + " at " + speed + " km/h");
    }
    private void checkMaxSpeed(Animal animal, double providedSpeed) {
        if(providedSpeed > animal.getMaxSpeed()) {
            throw new IllegalArgumentException("Maximum speed of " + animal.getName() +
                    " is: " + animal.getMaxSpeed() + ".\nSpeed provided: " + providedSpeed + ".");
        }
    }
    private void checkMaxAge(Animal animal, double providedAge) {
        if(providedAge > animal.getMaxAge()) {
            throw new IllegalArgumentException("Error: maximum age of " + animal.getName() +
                    " is " + animal.getMaxAge() + " years.\nAge provided: " + providedAge + ".");
        }
    }

    private void checkCommandlineArgs(String[] args) {
        if(args.length%2 != 0) {
            throw new IllegalArgumentException("Wrong input, please run with single word 'help'.");
        }
        for (int i = 1; i < args.length; i += 2) {
            try {
                Integer.parseInt(args[i]);
            } catch(Exception e) {
                throw new IllegalArgumentException("Please provide the age of the animal in whole years.");
            }
            if(Integer.parseInt(args[i]) < 0) {
                throw new IllegalArgumentException("Age cannot be negative!");
            }
        }
        for (int i = 0; i < args.length; i += 2) {
            try {
                Animals.valueOf(args[i].toLowerCase());
            } catch(Exception e) {
                throw new IllegalArgumentException("Error: animal species " + args[i] +
                        " is not known. run with single parameter \"help\" to get a listing of available species. Please give new values");
            }
        }
    }

    private void printHelp() {
        System.out.println("Usage: java AnimalSimulator <Species age Species age ...>\n" +
                "Supported species (in alphabetical order):");
        Animals[] animals = Animals.values();
        for(int i = 0; i < animals.length; i++) {
            String name = animals[i].toString();
            String cap = name.substring(0, 1).toUpperCase() + name.substring(1);
            System.out.println(i+1 + ": " + cap);
        }
    }
}
