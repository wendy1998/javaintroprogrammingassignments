/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal {

    Horse(int age) {
        this.age = age;
        this.maxAge = 62;
        this.maxSpeed = 88;
    }

    @Override
    public String getName() {
        return "Horse";
    }

    @Override
    public String getMovementType() {
        return "gallop";
    }
}
