package section4_oop.part2_palindrome_finder;

import java.util.ArrayList;

public class DNASeq {
    String sequence;
    SequenceType type;

    public DNASeq(String sequence) {
        this.sequence = sequence;
        this.type = SequenceType.Forward;
    }

    public static void main(String[] args) {
        DNASeq fw = new DNASeq("GGATAGCCGGGTATCC");
        ArrayList<Integer> wrongPositions = fw.CheckPalindrome();
        fw.PrintOutcome(wrongPositions);
    }

    private DNASeq(String sequence, String type) {
        this.sequence = sequence;
        this.type = SequenceType.Reverse;
    }

    private DNASeq CreateComplement() {
        String complement = "";
        for(String nuc: sequence.split("")) {
            switch (nuc) {
                case "A":
                    complement = complement.concat("T");
                    break;
                case "C":
                    complement = complement.concat("G");
                    break;
                case "T":
                    complement = complement.concat("A");
                    break;
                case "G":
                    complement = complement.concat("C");
                    break;
            }
        }
        return new DNASeq(complement, "reverse");
    }

    private String ReverseComplement() {
        String reverse = "";
        for(int i = sequence.length() -1; i >= 0; i--) {
            reverse = reverse.concat(sequence.split("")[i]);
        }
        return reverse;
    }

    public ArrayList<Integer> CheckPalindrome() {
        ArrayList<Integer> wrongPositions = new ArrayList<>();
        DNASeq complement = this.CreateComplement();
        String reverseComplement = complement.ReverseComplement();
        String[] fwList = this.sequence.split("");
        String[] rcList = reverseComplement.split("");

        for(int i=0; i < reverseComplement.length(); i++) {
            if(!(fwList[i].equals(rcList[i]))) {
                wrongPositions.add(i);
            }
        }

        return wrongPositions;
    }
    public void PrintOutcome(ArrayList<Integer> wrongPositions) {
        String lowercaseSeq = this.sequence.toLowerCase();
        String seqToPrint = "";
        String codeToPrint = "";
        for(int i = 0; i < this.sequence.length(); i++) {
            if(wrongPositions.contains(i)) {
                seqToPrint = seqToPrint.concat(lowercaseSeq.split("")[i]);
                codeToPrint = codeToPrint.concat("*");
            } else {
                seqToPrint = seqToPrint.concat(this.sequence.split("")[i]);
                if(i < this.sequence.length()/2) {
                    codeToPrint = codeToPrint.concat(">");
                } else {
                    codeToPrint = codeToPrint.concat("<");
                }
            }
        }
        System.out.println(seqToPrint + "\n" + codeToPrint);
    }


}
