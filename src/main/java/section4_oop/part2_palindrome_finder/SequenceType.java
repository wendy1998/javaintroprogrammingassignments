package section4_oop.part2_palindrome_finder;

public enum SequenceType {
    Forward,
    Reverse;
}
