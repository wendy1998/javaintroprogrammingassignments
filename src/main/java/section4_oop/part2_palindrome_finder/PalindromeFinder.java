package section4_oop.part2_palindrome_finder;

import java.util.ArrayList;

public class PalindromeFinder {
    public static void main(String[] args) {
        PalindromeFinder pf = new PalindromeFinder();
        pf.start(args);
    }

    private void start(String[] args) {
        /*commandline arguments processing*/
        /*check if arguments are given*/
        if(args.length == 0) {
            printHelp();
            return;
        }
        /*check whether help was requested*/
        if(args[0].toLowerCase().equals("help")) {
            printHelp();
            return;
        }

        /*check arguments*/
        try {
            checkArguments(args);
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }
        /*check for valid nucleotides*/
        try {
            checkValidNucs(args);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }

        /*perform analysis*/
        for(int i=0; i < args.length; i+=2) {
            String sequence = args[i].toUpperCase();
            int k = Integer.parseInt(args[i+1]);

            DNASeq forward = new DNASeq(sequence);
            ArrayList<Integer> wrongPositions = forward.CheckPalindrome();

            if(wrongPositions.size() > k) {
                System.out.println("This is not a " + k + "-palindrome.\nNumber of wrong positions: " + wrongPositions.size());
            } else {
                System.out.println("This classifies as a " + k + "-palindrome! Palindrome:");
                forward.PrintOutcome(wrongPositions);
            }
        }


    }

    private void checkArguments(String[] args) {
        if(args.length%2 != 0) {
            throw new IllegalArgumentException("Error: Please provide a threshold for every DNA sequence.");
        }
        for(int i = 1; i < args.length; i+=2) {
            try {
                Integer.parseInt(args[i]);
            } catch (Exception e) {
                throw new IllegalArgumentException("Error: Please provide the threshold as a whole number.");
            }
            int threshold = Integer.parseInt(args[i]);
            if(threshold < 0 || threshold > args[i-1].length()) {
                throw new IllegalArgumentException("Error: Wrong threshold size, " +
                        "please provide a threshold bigger or equal to 0 and smaller or " +
                        "equal to the length of the accompanying sequence.");
            }
        }
    }

    private void checkValidNucs(String[] args) {
        for(int i=0; i < args.length; i+=2) {
            int flag = 0;
            String[] sequence = args[i].toUpperCase().split("");
            for(String nuc: sequence) {
                if(!(nuc.equals("A") | nuc.equals("C") | nuc.equals("T") | nuc.equals("G"))) {
                    flag = 1;
                }
            }
            if(flag == 1) {
                throw new IllegalArgumentException("Invalid nucleotide used.\n" +
                        "Valid nucleotides are: A, C, T, G.");
            }
        }
    }

    private void printHelp() {
        System.out.println("Usage: java PalindromeFinder <DNAseq k-threshold DNAseq k-threshold...>.\n" +
                "Only provide DNA sequences with nucleotides ACTG.");
    }
}
