/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package section2_syntax.part3_flow_control;

import java.util.Arrays;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("GATCG", true, true); //should print left truncated, left aligned
    }

    /**
     * Prints all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(String stringToSubstring, boolean leftTruncated, boolean leftAligned) {
        int len_string = stringToSubstring.length();
        String space = "";
        for (int i = 0; i < len_string; i++) {
            if (! leftAligned) {
                char[] spaces = new char[i];
                Arrays.fill(spaces, ' ');
                space = new String(spaces);
            }
            if (leftTruncated) {
                if (! leftAligned) {
                    System.out.println(space + stringToSubstring.substring(i));
                } else {
                    System.out.println(stringToSubstring.substring(i));
                }
            } else {
                if (! leftAligned) {
                    System.out.println(space + stringToSubstring.substring(0, len_string-i));
                } else {
                    System.out.println(stringToSubstring.substring(0, len_string-i));
                }
            }
        }

    }
}
